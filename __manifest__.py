# -*- encoding: utf-8 -*-
{
    "name": "Chile Localization - VAT validation, cities and states",
    'version': '10.0.0.0.0',
    "category": "Localization",
    "author": "Tejesoft SPA",
    "website": "http://tejesoft.com",
    'license': 'AGPL-3',
    'summary': '',
    "description": """Agrega datos de RUT, regiones, provincias y comunas para localizacion de Chile""",
    "depends": [
        "base", "base_vat"
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/res_partner_view.xml",
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
