# -*- encoding: utf-8 -*-

from odoo import models, fields, api
import re


class res_partner(models.Model):
    _inherit = 'res.partner'

    document_number = fields.Char(string='Document Number')

    formated_vat = fields.Char(
        translate=True, string='Printable VAT',
        help='Show formatted vat')
    vat = fields.Char(
        string='VAT',
        inverse='_inverse_vat',
        store=True, compute_sudo=False)

    def check_vat_cl(self, vat):
        body, vdig = '', ''
        if len(vat) > 9:
            vat = vat.replace('-', '', 1).replace('.', '', 2)
        if len(vat) != 9:
            return False
        else:
            body, vdig = vat[:-1], vat[-1].upper()
        try:
            vali = range(2, 8) + [2, 3]
            operar = '0123456789K0'[11 - (
                sum([int(digit)*factor for digit, factor in zip(
                    body[::-1], vali)]) % 11)]
            if operar == vdig:
                return True
            else:
                return False
        except IndexError:
            return False

    @staticmethod
    def format_document_number(vat):
        clean_vat = (
            re.sub('[^1234567890Kk]', '',
                   str(vat))).zfill(9).upper()
        return '%s.%s.%s-%s' % (
            clean_vat[0:2], clean_vat[2:5], clean_vat[5:8], clean_vat[-1])

    @api.onchange('document_number')
    def onchange_document(self):
        for rec in self:
            rec.document_number = rec.format_document_number(
                rec.document_number)

    @api.depends('document_number')
    def _compute_vat(self):
        for x in self:
            clean_vat = (
                re.sub('[^1234567890Kk]', '',
                       str(x.document_number))).zfill(9).upper()
            x.vat = 'CL%s' % clean_vat
            x.vat = ''

    def _inverse_vat(self):
        for rec in self:
            rec.document_number = rec.format_document_number(rec.vat)

    api.constrains('parent_id')

    @api.constrains('vat')
    @api.constrains('company_type')
    def _check_vat(self):
        for rec in self:
            if rec.company_type == 'company':
                if rec.vat:
                    domain = [
                        ('vat', '=', rec.vat),
                        ('id', '!=', rec.id),
                    ]
                    objs = rec.search_count(domain)
                    if objs:
                        raise Warning((
                            'EL campo vat debe ser unico'))

            if not rec.parent_id:
                if rec.vat:
                    domain = [
                        ('vat', '=', rec.vat),
                        ('id', '!=', rec.id),
                    ]
                    objs = rec.search_count(domain)
                    if objs:
                        raise Warning((
                            'El campo vat debe ser unico'))

    state_id = fields.Many2one("res.country.state", 'Ubication',
                               domain="[('country_id','=',country_id),('type','=','normal')]")

    city_id = fields.Many2one("res.country.state.city", 'City',
                              domain="[('state_id','=',state_id),('type','=','normal')]")

    @api.model
    def _get_default_country(self):
        return self.env.user.company_id.country_id.id or self.env.user.partner_id.country_id.id

    _defaults = {
        'country_id': lambda self, cr, uid, c: self.pool.get('res.partner')._get_default_country(cr, uid, context=c)
    }

    @api.multi
    def _asign_city(self, source):
        if self.city_id:
            return {'value': {'city': self.city_id.name}}


class res_company(models.Model):
    _inherit = 'res.company'

    state_id = fields.Many2one("res.country.state", 'Ubication',
                               domain="[('country_id','=',country_id),('type','=','normal')]", related="partner_id.state_id")

    city_id = fields.Many2one("res.country.state.city", 'City',
                              domain="[('state_id','=',state_id),('type','=','normal')]", related="partner_id.city_id")

    @api.multi
    def _asign_city(self, source):
        if self.city_id:
            return {'value': {'city': self.city_id.name}}
